<!DOCTYPE html>
<html lang="en">
<head>
  
 <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="style.css" rel="stylesheet">
</head>
<body>
 

 <?php
        include_once 'header.php';
?>

<br>
 <main class ="floral-img-3">

<div class="container-fluid padding section-about">
  <div class="row padding">
    <div class="col-lg-6">
      <h2>About Rosie's Soaps</h2>
      <p>We are a small family owned business operating in Dublin Ireland. We are committed to producing high quality organic soap and beauty products. Since our founding in 2013 we have expanded from producing soaps into haircare and skincare. </p>
    </div>
    <div id ="boxshadow" class="col-lg-6">
      <img src="img/57bcbe0d95fb9aa9ec40ceecae42ff05.jpeg" class="img-fluid">
    </div>
    </div>
  </div>
   </main>>

<hr>
<br>


  <div class="container section-ourTeam">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 ourTeam-heading text-center">
      <h1>Meet Our Team</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="row section-success ourTeam-box text-center">
        <div class="col-md-12 section1">
          <img src="img/c1-0211-soap-0305-jpg.jpg">
        </div>
        <div class="col-md-12 section2">
          <p>Rosie Philips</p><br>
          <h1>Owner</h1><br>
        </div>
        <div class="col-md-12 section3">
          <p>
            
          </p>
        </div>
        <div class="col-md-12 section4">
          <i class="fa fa-facebook-official" aria-hidden="true"></i>
          <i class="fa fa-twitter" aria-hidden="true"></i>
          <i class="fa fa-google-plus" aria-hidden="true"></i>
          <i class="fa fa-envelope" aria-hidden="true"></i>
        </div>
      </div>
    </div>


    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="row section-info ourTeam-box text-center">
        <div class="col-md-12 section1">
          <img src="img/web1_161207-SWR-Cheesin-MED.jpg">
        </div>
        <div class="col-md-12 section2">
          <p>Harry Phillips</p><br>
          <h1>Co-founder</h1><br>
        </div>
        <div class="col-md-12 section3">
          <p>
        
          </p>
        </div>
        <div class="col-md-12 section4">
          <i class="fa fa-facebook-official" aria-hidden="true"></i>
          <i class="fa fa-twitter" aria-hidden="true"></i>
          <i class="fa fa-google-plus" aria-hidden="true"></i>
          <i class="fa fa-envelope" aria-hidden="true"></i>
        </div>
      </div>
    </div>


    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="row section-danger ourTeam-box text-center">
        <div class="col-md-12 section1">
          <img src="img//5bdb6ad3a83ba399f603dc1d6e12b2d9.jpg">
        </div>
        <div class="col-md-12 section2">
          <p>Laura McDaid</p><br>
          <h1>Co-founder</h1>
        </div>
        <div class="col-md-12 section3">
          <p>
          </p>
        </div>
        <div class="col-md-12 section4">
          <i class="fa fa-facebook-official" aria-hidden="true"></i>
          <i class="fa fa-twitter" aria-hidden="true"></i>
          <i class="fa fa-google-plus" aria-hidden="true"></i>
          <i class="fa fa-envelope" aria-hidden="true"></i>
        </div>
      </div>
    </div>
  </div>
</div>


</body>

 <?php
        include_once 'footer.php';
?>
</html>