(function($) {

    'use strict';

    var pluginName = 'slider',
        defaults = {
            next: '.slider-nav__next',
            prev: '.slider-nav__prev',
            item: '.slider__item',
            dots: false,
            dotClass: 'slider__dot',
            autoplay: false,
            autoplayTime: 3000,
        };

    function slider(element, options) {
        this.$document = $(document);