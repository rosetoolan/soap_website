-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 31, 2018 at 12:20 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cart`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `totalprice` varchar(255) NOT NULL,
  `orderstatus` varchar(255) NOT NULL,
  `paymentmode` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `uid`, `totalprice`, `orderstatus`, `paymentmode`, `timestamp`) VALUES
(1, 2, '80', 'Order Placed', 'cod', '2017-10-28 12:22:36'),
(2, 2, '12.49', 'Order Placed', 'cod', '2017-10-28 12:27:16'),
(3, 6, '37.89', 'Cancelled', 'cod', '2017-10-28 14:25:23'),
(4, 6, '10.99', 'In Progress', 'cod', '2017-10-28 14:28:29'),
(5, 6, '56.99', 'In Progress', 'cod', '2017-11-06 19:40:34');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `categories` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `image`, `price`, `categories`) VALUES
(1, 'Rose Quartz Soap', 'img/Rose-Quartz-Soap-Tutorial.jpg', 5.99, 'soaps'),
(2, 'Lavendar Soap', 'img/12bb152bcc094fd2d1ef90980015eebd.jpg', 5.99, 'soaps'),
(3, 'Lavendar Bath Bombs', 'img/il_570xN.258056972.jpg', 5.99, 'bath_bombs'),
(4, 'Mermaid Soap', 'img/Mermaid-Tail-Cold-Process.jpg', 5.99, 'soaps'),
(5, 'Lemon Soap', 'img/diy-soap-lemon-cake-homemade-soap-recipes.jpg', 5.99, 'soaps'),
(6, 'Coconut Shampoo', 'img/Hair-care.jpg', 5.99, 'haircare'),
(7, 'Peach Soap', 'img/4.jpg', 5.99, 'soaps'),
(8, 'Blueberry Soap', 'img/5.jpg', 5.99, 'soaps'),
(9, 'Rose Petal Bathbomb', 'img/7.jpg', 5.99, 'bath_bombs'),
(10, 'Flower Power Bathbomb', 'img/9.jpg', 5.99, 'bath_bombs'),
(11, 'Aquamarine Soap', 'img/3.jpg', 5.99, 'soaps'),
(12, 'Rosehip Face Serum', 'img/16.jpg', 15.99, 'skincare'),
(13, 'Witch Hazel Moisturizer', 'img/19.jpg', 19.99, 'skincare'),
(14, 'Fennel Shampoo', 'img/14.jpg', 19.99, 'haircare'),
(15, 'Fennel Conditioner', 'img/15.jpg', 19.99, 'haircare');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
