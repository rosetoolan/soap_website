-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 31, 2018 at 12:19 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loginsystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_first` varchar(255) NOT NULL,
  `user_last` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_uid` varchar(255) NOT NULL,
  `user_pwd` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_first`, `user_last`, `user_email`, `user_uid`, `user_pwd`) VALUES
(1, 'gmokre', 'sfwfs', 'fowejowe@hues.com', 'nafnew', '$2y$10$Qh1fTHTTQT1aHq0U8XPapOEVU9yCit/VaD10bJwL8407S7bER7MY2'),
(2, 'Rose', 'Toolan', 'rosetoolan@hotmail.com', 'rose217', '$2y$10$B2jusbcfqWz4TPA1ju.SSOd857pdAoA3QelO1wDXe/P6niXcw1tWi'),
(3, 'wtw', 'ert', 'rose.toolan@gmail.com', 'rose.toolan@gmail.com', '$2y$10$1qGNTgJtJnpy1LE3vImKWOCWAgnS6fVTTGa9eaoGBTQErDyvbv3tm'),
(4, 'dsadas', 'hhkj', 'hkhh', 'hkhkhk', '$2y$10$Y2MyT7Le3KyLPNT3XezyKOQh98dBI1xrQ1uKptqLS/klt6igWBfXi'),
(5, 'Gerry', 'mcbride', 'gerardamcbride@gmail.com', 'gerardamcbride@gmail.com', '$2y$10$Lt7tIg3XJTF/EWKZCxVnlee0JimjxOl.zpvwi3IMfxa4gMHUKOL8O'),
(6, 'rose', 'toolan', 'rose.toolan@yahoo.com', 'rose.toolan@yahoo.com', '$2y$10$itKY2x/ufcO9futLJyKFieY10tragZ0P93/eE9xkp/juYrfIOEf06'),
(7, 'ROse', 'toolan', 'gerardmcbride@gmail.com', 'gerardmcbride@gmail.com', '$2y$10$rK.TLBgUK8IZ2tbnhYVk5e0YyOuvQ.MHd4ueVvjbZzbfl1opMZa/C'),
(8, 'Rose', 'Toolan', 'rosetoolan@gmail.com', 'rosetoolan@gmail.com', '$2y$10$.tavTlBuTYYbFvhtLq5I2OpKkUEUzt8prDqfEwq3B4GMfTG4mSYee');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
