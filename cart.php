<style>
.order-container h3{

  font-family: 'Amatic SC', cursive;
    font-size:50px;
    font-weight: 300;
    color: #EB8FCC;
    text-shadow: 4px 4px 16px #EBB0D0;
  
}

.order-container th{

  font-family: 'Amatic SC', cursive;
    font-size:20px;
    font-weight: 700;
   color: #463080;
  
}

.order-container td{

  font-family: 'Amatic SC', cursive;
    font-size:18px;
    font-weight: 500;
   color: #463080;
  
}

.order-container button{
    display: block;
    width:10%;
    height: 50px;
    border: none;
    margin-bottom: 4px;
    background-color: #EBB0D0;
    font-family:'Amatic SC', cursive;
    font-size: 30px;
    font-weight: 500;
    color: #fff;
    line-height: 12px;
    text-shadow: 4px 4px 16px #A87798;

}

.order-container button:hover{
    background-color: #F7F5FC;
    border: 2px solid;
    border-color: #EBB0D0;
    cursor: pointer;
    color:#463080;
    line-height: 12px;
    }

  

</style>
<?php
session_start();
$product_ids = array();
$is_prod = false;
//session_destroy();

//check if Add to Cart button has been submitted
if(filter_input(INPUT_POST, 'add_to_cart')){
   $is_prod = false;
    if(isset($_SESSION['shopping_cart'])){
        
        //keep track of how mnay products are in the shopping cart
        $count = count($_SESSION['shopping_cart']);
        
        //create sequantial array for matching array keys to products id's
        $product_ids = array_column($_SESSION['shopping_cart'], 'id');
        
        if (!in_array(filter_input(INPUT_GET, 'id'), $product_ids)){
        $_SESSION['shopping_cart'][$count] = array
            (
                'id' => filter_input(INPUT_GET, 'id'),
                'name' => filter_input(INPUT_POST, 'name'),
                'price' => filter_input(INPUT_POST, 'price'),
                'quantity' => filter_input(INPUT_POST, 'quantity')
            );   
        }
        else { //product already exists, increase quantity
            //match array key to id of the product being added to the cart
            for ($i = 0; $i < count($product_ids); $i++){
                if ($product_ids[$i] == filter_input(INPUT_GET, 'id')){
                    //add item quantity to the existing product in the array
                    $_SESSION['shopping_cart'][$i]['quantity'] += filter_input(INPUT_POST, 'quantity');
                }
            }
        }
        
    }
    else { //if shopping cart doesn't exist, create first product with array key 0
        //create array using submitted form data, start from key 0 and fill it with values
        $_SESSION['shopping_cart'][0] = array
        (
            'id' => filter_input(INPUT_GET, 'id'),
            'name' => filter_input(INPUT_POST, 'name'),
            'price' => filter_input(INPUT_POST, 'price'),
            'quantity' => filter_input(INPUT_POST, 'quantity')
        );
    }
} 

#elseif (filter_input(INPUT_POST, 'open_product')) {
  # code...
 #       $connect = mysqli_connect('localhost', 'root', '', 'cart');
  #      $query = $connect->prepare('SELECT * FROM products WHERE id = ?');
   #     $query->bind_param("s", $id);
    #    $id = filter_input(INPUT_GET, 'id');
#
 #       $result = $query->execute();
  #      $is_prod = true;
#}

if(filter_input(INPUT_GET, 'action') == 'delete'){
    //loop through all products in the shopping cart until it matches with GET id variable
    foreach($_SESSION['shopping_cart'] as $key => $product){
        if ($product['id'] == filter_input(INPUT_GET, 'id')){
            //remove product from the shopping cart when it matches with the GET id
            unset($_SESSION['shopping_cart'][$key]);
        }
    }
    //reset session array keys so they match with $product_ids numeric array
    $_SESSION['shopping_cart'] = array_values($_SESSION['shopping_cart']);
}

//pre_r($_SESSION);

function pre_r($array){
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Shopping Cart</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
    	 <?php
        include_once 'header.php';
?>
 
        <div class="container order-container">
        
        <div style="clear:both"></div>  
        <br />  
        <div class="table-responsive">  
        <table class="table">  
            <tr><th colspan="5"><h3>Order Details</h3></th></tr>   
        <tr>  
             <th width="20%">Product Name</th>  
             <th width="10%">Quantity</th>  
             <th width="15%">Price</th>  
             <th width="15%">Total</th>  
             <th width="5%">Action</th>  
        </tr>  
        <?php
        if($is_prod):
          header( "Location: http://stackoverflow.com" );
          die();

          $product = $result;

          
        elseif(!empty($_SESSION['shopping_cart'])):  
             $itemstring = '';
             $total = 0;  
        
             foreach($_SESSION['shopping_cart'] as $key => $product):
             $itemstring = $itemstring .= $product['name'];
             $itemstring = $itemstring .= '_';
        ?>  
        <tr>  
           <td><?php echo $product['name']; ?></td>  
           <td><?php echo $product['quantity']; ?></td>  
           <td>$ <?php echo $product['price']; ?></td>  
           <td>$ <?php echo number_format($product['quantity'] * $product['price'], 2); ?></td>  
           <td>
               <a href="cart.php?action=delete&id=<?php echo $product['id']; ?>">
                    <div class="btn-danger">Remove</div>
               </a>
           </td>  
        </tr>  
        <?php  
                  $total = $total + ($product['quantity'] * $product['price']);  
             endforeach;  
        ?>  
        <tr>  
             <td colspan="3" align="left" style="font-weight:700;">Total</td>  
             <td align="left" style="font-weight:700;">$ <?php echo number_format($total, 2); ?></td>  
             <td></td>  
        </tr>  
        <tr>
            <!-- Show checkout button only if the shopping cart is not empty -->
            <td colspan="5">
             <?php 
                if (isset($_SESSION['shopping_cart'])):
                if (count($_SESSION['shopping_cart']) > 0):
             ?>
             <br>
             <a href="checkout.php?total=<?php echo $total ?>&items=<?php echo $itemstring ?>">

              <button style="wisth:100%;" type="button" class="btn btn-outline-secondary btn-lg">Check Out</button></a>
           
             <?php endif; endif; ?>
            </td>
        </tr>
        <?php  
        endif;
        ?>  
        </table>  
         </div>
        </div>
    </body>
     <?php
        include_once 'footer.php';
?>
</html>