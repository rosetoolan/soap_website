<!DOCTYPE html>
<html lang="en">
<head>
  
 <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="style.css" rel="stylesheet">
</head>
<body>
 

 <?php
        include_once 'header.php';
?>



 <main class ="floral-img-3">

<div class="container-fluid padding section-about">
  <div class="row padding">
    <div class="col-lg-12">
      <h2>DELIVERY AND RETURNS</h2>

        <h3>1 Introduction</h3>

<p>1.1 These terms and conditions (the “Terms”) are the terms on which this website https://www.thehandmadesoapcompany.ie  (the “Website”) is made available to you (“You”/”Your”). Please read these Terms carefully before using Our Website.</p>

<p>1.2 By using or accessing Our Website, You agree to be legally bound by these Terms as they apply to Your use of or access to Our Website.</p>

<p>1.3 If You do not wish to be bound by these Terms then please refrain from using Our Website.</p>

<p>1.4 Any products which are available for You to purchase on or via Our Website are subject to additional terms and conditions which will be notified to You when You follow the ordering procedures set out on the relevant order page.</p>

<h3>2 Information about Us</h3>

<p>2.1 We are The Handmade Soap Company Ltd a company registered in The Republic of Ireland under registration number 513872  and our registered address is The Old Mill, Slane, Co Meath, The Republic of Ireland (“We”/“Us”/”Our”).</p>

<p>2.2 If You have any questions, complaints or comments on this Website then You may contact Us on info@thehandmadesoapcompany.ie or by post to The Handmade Soap Company, The Old Mill, Slane, Co Meath, The Republic of Ireland .</p>

<p>2.3 Our VAT number is IE 9838821S.</p>

<h3>3 Buying Products on our Website</h3>

<p>3.1 To order a product You will need to follow the ordering procedures set out on the relevant order page.</p>

<P>3.2 Details of the prices for the products, and the procedures for payment and delivery are displayed on or through Our Website. The price of any product is the price in force at the date and time of Your order. The price of any product includes VAT but excludes any delivery charge. We are entitled to make adjustments to the Price to take account of any increase in our supplier’s prices, or the imposition of any new taxes or duties, or if due to an error or omission the Price for the Products on the Website is wrong.</P>

<p>3.3 You undertake that all details You provide to Us for the purpose of purchasing goods which may be offered by Us on Our Website will be correct, that the credit or debit card, or any electronic cash, which You use is Your own and that there are sufficient funds or credit facilities to cover the cost of any goods. We reserve the right to obtain validation of Your credit or debit card details from Your card issuer before providing You with any goods .</p>

<p>3.4. Discount codes and offers cannot be redeemed on gift vouchers, sale, promotional items or living.</p>

<p>3.5 From time to time we will make promotional codes available. Only one code may be used per transaction. We will alert You to additional specific terms which apply to such promotional codes at the relevant time.</p>

<h3>4 The Handmade Soap Company Products</h3>

<p>4.1 You must pay for the products by credit, debit card or Paypal account at the time of the order, at which time You will be notified of the current price.</p>

<p>4.2 We are entitled to refuse any order placed by You. If Your order is accepted, We will confirm acceptance to You by online electronic means ('Confirmation') to the e-mail address You have given Us on ordering. The order will then be fulfilled by the date set out in the Confirmation.</p>

<p>4.3 To cancel Your order after it has been dispatched to You, you will need to follow the Returns Policy & Procedure.</p>

<h3>5 Reselling of products </h3>

<p>5.1 Please note that all products available on Our Website are for personal use only. You may not sell or resell any of the products.</p>

<p>5.2 We reserve the right to cancel or reduce the quantity of any orders that we believe may result in the violation of our Terms.</p>

<h3>6 Information you provide</h3>

<p>6.1 The following applies to any information You provide to us, for example during any registration or ordering process:</p>

<p>You authorise us to use, store or otherwise process any personal information which relates to and identifies You, including but not limited to Your name and address, to the extent reasonably necessary to provide the services which are available through Our Website by Us or our sub-contractors. If You obtain or choose to buy products through Our Website then We may collect information about Your buying behaviour and if You send Us personal correspondence such as e-mails or letters then We may collect this information into a file specific to You. All such information collected by Us shall be referred to in these Terms as “Personal Information”;
You must ensure that the Personal Information You provide is accurate and complete and that all ordering or registration details (where applicable) contain Your correct name, address and other requested details. </p>
 

<p>6.2 You must read Our Privacy Policy which contains important information about the use of Your Personal Information and other information regarding Your Privacy.</p>


<p>6.3 If You would like to review or modify any part of Your Personal Information then You should e-mail Us at info@thehandmadesoapcompany.ie</p>

<h3>7 Security</h3>

<p>You are solely responsible in all respects for all use of and for protecting the confidentiality of any username and password that may be given to You or selected by You for use on Our Website. You may not share these with or transfer them to any third parties. You must notify Us immediately of any unauthorised use of them or any other breach of security regarding Our Website that comes to Your attention.</p>

<h3>8 Applicability of Online Materials</h3>

<p>8.1 Personal Use
Unless otherwise stated, text on our website is presented solely for your private, personal and non-commercial use.</p>

<p>8.2 Exclusion of Warranties
We make no warranties, express or implied that making the products available in any particular jurisdiction outside the The Republic of Ireland is permitted under any applicable non-Irish laws or regulations. Accordingly, if making the products or any part available in Your jurisdiction or to You (by reason of nationality, residence or otherwise) is prohibited, those products are not offered for sale to You. You accept that if you are resident outside The Republic of Ireland, You must satisfy yourself that You are lawfully able to purchase the products. We accept no liability, to the extent permitted by applicable law, for any costs, losses or damages resulting from or related to the purchase or attempted purchase of the products by persons in jurisdictions outside The Republic of Ireland or who are nominees of or trustees for citizens, residents or nationals of other countries.<p>

<h3>9 Copyright</h3>

<p>9.1 Your use of Our Website and its contents grants no rights to You in relation to Our intellectual property rights including, without limitation, trade marks, logos, graphics, photographs, animations, videos and text or the intellectual property of third parties in Our Website and its contents.</p>

<p>9.2 You may not copy, reproduce, republish, download, reproduce, republish, download, post, broadcast, record, transmit, commercially exploit, edit, communicate to the public or distribute in any way the services, web pages or materials on the Website or the computer codes of elements comprising the Website other than for Your own personal use. Subject to the above, You may download insubstantial excerpts of this content to Your hard disk for the purpose of viewing it provided that no more than one copy of any information is made.</p>

<p>9.3 Any use other than that permitted above may only be undertaken with Our prior express authorisation.</p>

<p>9.4 By submitting information, text, photos, graphics or other content to Us via Our Website, You grant Us a right to use such materials at Our own discretion including, without limitation, to edit, copy, reproduce, disclose, post and remove such materials from Our Website.</p>

<h3>10 Linking and Use of Cookies</h3>

<p>10.1 You may establish links to the Website provided:</p>

<p>You link only to the home page of the Website;
You do not remove or obscure advertisements, the copyright notice or other notices on the Website;
You give us notice of such link by sending an e-mail message to us at info@thehandmadesoapcompany.ie and
You immediately stop providing links to the Website if notified by Us.</p>
 

<p>10.2 We make no representations whatsoever about any other websites which You may access through Our Website or which may link to Our Website. These links are provided for Your ease of reference and convenience only. When You access any other website it is independent from Us. We have no control over the content or availability of that website. In addition, a link to any other site does not mean that We endorse or accept any responsibility for the content, or the use of, such a website and shall not be liable for any loss or damage caused or alleged to be caused by or in connection with use of or reliance on any content, goods or services available on or through any other website or resource. You agree that You will not involve Us in any dispute between You and the third party. Any concerns regarding any external link should be directed to its website administrator or web master.</p>

<h3>10.3 Use of Cookies</h3>
<p>10.3.1 As a result of viewing Our Website We may log certain information about You by using cookies. Cookies are small files which are stored upon the hard drive of Your computer. We may use cookies to:<p>

<p>measure the number of visitors to Our Website;
see how visitors navigate Our Website and to see which resources they access;
see what visitors’ buying behaviors are to allow us to see the latest buying trends; and
see information about visitors’ internet connections and the equipment used to access Our Website.</p>
 

<P>10.3.2 We use this information to help Us to improve the content of Our Website or to improve the matching of Your interests or preferences.</P>

<p>10.3.3 The information We collect using cookies does not identify You personally. For example, it does not identify Your email address. It may include the IP address and/or the domain name of the computer You use. You should be aware that this information might therefore also identify (i) the company You work for (if You access Our Website from Your place of work), or (ii) Your Internet Service Provider. It may also identify information about Your computer, for example Your operating system and browser type.</p>

<p>10.3.4 If You access Our Website by way of a hardware firewall, a proxy server or any other kind of router (such as a dial-up modem connection at Your ISP) then it is the address of the router that will appear in Our logs and not the address of Your computer.</p>

<p>10.3.5 If You do not want a cookie to be stored in Your computer, most Internet browsers have functions to erase cookies from the computer's hard drive or to block all cookies or to receive a warning before a cookie is stored. You are welcome to use such facilities to prevent the installation of any cookie. If You refuse to accept cookies You may be unable to access certain parts of Our Website.</p>

<h3>11 Your Use of the Website</h3>

<p>11.1 You agree that in using Our Website You will not:use Our Website in any way that may lead to the encouragement, procurement or carrying out of any criminal activity;
use Our Website in any way that interrupts, damages, impairs or renders the Website less efficient;
use Our Website for any purpose other than Your personal use;
email, transmit or otherwise disseminate any content which is defamatory, obscene, in breach of copyright, vulgar or indecent or may have the effect of being harassing; threatening, abusive or hateful or that otherwise degrades or intimidates an individual or group of individuals on the basis of religion, gender, sexual orientation, race, ethnicity, age or disability;
advertise or promote third party or Your own products or services including by way of the distribution of ‘spam’ email;
transfer files that contain viruses, trojans or other harmful programs; or
access or attempt to access the accounts of other users or to penetrate or attempt to penetrate Our Website security measures.</p>
 

<p>11.2 We reserve the right to suspend, restrict or terminate Your access to Our Website at any time without notice at Our discretion if we have reasonable grounds to believe You have breached any of the restrictions above.</p>

<h3>11.3 You confirm that:</h3>

<p>You will comply with the restrictions on Your use of the Website as set out in these Terms; and
in relation to any material submitted to or posted on the Website You have the right to do so and have obtained all necessary licences and or approvals.</p>
 

<p>11.4 You agree to compensate Us from any claim or damages (including any legal fees in relation to such claim or damages) made by a third party in respect of any matter in relation to or arising from Your use of Our Website including any breach or suspected breach of these Terms or Your violation of any law or the rights of a third party.</p>

<h3>12 Availability of our Website</h3>

<p>12.1 We will try to make Our Website available but cannot guarantee that Our Website will operate continuously or without interruptions or be error free and can accept no liability for its unavailability. </p>

<p>12.2 Access to Our Website may be suspended temporarily and without notice in the case of system failure, maintenance or repair or for reasons reasonably beyond Our control.</p>

<h3>13 Liability</h3>

<p>13.1 Our liability in tort, contract, negligence, pre-contract or other representations or otherwise arising out of or in connection with these Terms shall be limited in aggregate to the net payment to Us from You for the product concerned.</p>

<p>13.2 All content and services on Our Website are provided on an 'as is' and 'as available' basis. We do not make any representation or give any warranty (whether express or implied) in respect of Our Website or its content, including, without limitation, any advice given (on a personal or general basis) and statements made by advertisers on or via the Website.  Nothing in these Terms shall restrict Your statutory rights (including Your rights to receive a reasonable standard of service).</p>

<p>13.3 . We do not accept liability for damage to Your computer system or loss of data that results from Your use of the Website. We do not guarantee or warrant that any material available for downloading from Our Website will be free from infection, viruses and/or other code that has contaminating or destructive properties. You are responsible for implementing sufficient procedures and virus checks (including anti-virus and other security checks) to satisfy Your particular requirements for the accuracy of data input and output. You are responsible for ensuring that Your computer system meets all relevant technical specifications necessary to use Our Website and is compatible with Our Website.</p>

<h3>14 General</h3>

<p>14.1 Alterations
We may alter these Terms from time to time and post the new version on Our Website, following which all use of Our Website will be governed by that version. You will be deemed to have accepted any such changes by Your use of Our Website from such time. You must check the terms and conditions on Our Website regularly.</p>

<p>14.2 Illegality
If any provision of these Terms is found by a court or regulator to be illegal, unlawful, invalid or unenforceable the other provisions shall continue to apply.</p>

<p>14.3 Jurisdiction
In the event of any dispute between You and Us concerning these Terms, relevant Irish and European law will apply. If You wish to take court proceedings against Us You must do so within The Republic of Ireland. If You are buying Our products from anywhere outside the Republic of Ireland this does not deprive You of the protection afforded to You by virtue of local laws in that territory.</p>

<p>14.4 Causes beyond Control
Neither you nor The Handmade Soap Company Ltd will be held liable for any failure to perform any obligation to the other due to causes beyond Your or Our respective reasonable control.</p>

<p>14.5 No Waiver
If you breach these Terms and We take no action against You, We will still be entitled to use Our rights and remedies in any other situation where You breach these Terms.</p>

 

<h3>15 Notices</h3>

<p>All notices shall be given:</p>

<p>to Us via e-mail at info@thehandmadesoapcompany.ie ; or
to You at either the e-mail or postal address you provide during any ordering process.</p>
 

<p>Notice will be deemed received when an e-mail is received in full (or else on the next business day if it is received on a weekend or a public holiday in the place of receipt) or 3 days after the date of posting.</p>

<h3>16 Replacement</h3>h3>

<p>These Terms replace all other terms and conditions previously applicable to the use of Our Website. </p>

<h3>17 Advertising</h3>

<p>We reserve the right at all times to place advertisements and promotions on the Website. Advertisers and sponsors on the Website are solely responsible for complying with all national and international laws (where relevant) and we exclude all liability howsoever arising there from.</p>

 

<h3>18 Newsletter and Targeted Advertising</h3>
<p>When you buy from The Handmade Soap Company we'll sign you up for our e-mail newsletter, full of great news and special offers.  These are only sent quarterly or monthly.  If you want to unsubscribe, you can at any time by clicking the 'unsubscribe link' of the newsletters we send you.</p>

<p>We may engage in Targeted Advertising which allows us to develop personalised advertising so that it directly relates to offers that may be of interest to you. Advertisements are placed so as to reach consumers based on various traits such as demographics and consumer behavioural variables (such as product purchase history). Targeted Advertising enables us to more easily determine user preferences and purchasing habits, so that ads will more pertinent and useful for the consumers. If you would like to opt out of receiving such advertising, please click the “unsubscribe” link in the advertisement and follow the instructions.</p>

<h3>19 Privacy Policy</h3>
<p>We understand that the details you supply us are to permit you purchasing Products on this Website and we will protect that information and use it only for the purpose for which it was intended. When you provide us with personally identifiable information through this Website, we respect your privacy and comply with our obligations under the Data Protection Acts 1988 2003. We may use your information to tell you about products, promotions and special offers that may be of interest to you, by email, mail or phone. All customers are given the opportunity to opt-out of receiving such further information at the time that they give us their Personal Information.</p>

 

<h3>20 Personal Data</h3>
<p>Any personal information which you provide to us will be securely stored, strictly in accordance with the Data Protection Acts, 1988 - 2003.</p>

 

<p>20.1 We will process any personal data you provide us for the following purposes: (a) to provide you with the Products you have ordered; (b) to contact you if required in connection with your order or to respond to any communications you might send us; (c) to contact you with information on special offers, advertisements or promotional information; and (d) if we are required to do so in order to comply with any applicable law, a summons, a search warrant, a court or regulatory order or other statutory requirement.</p>

 

<p>20.2 When you submit your personal data, it may be shared with a reputable third party ("Third Party Processors"). These Third Party Processors may include, for example, companies that store our data. The data we provide to Third Party Processors is securely stored and maintained by them so that it can be accessed when needed for future purposes. Further, all such Third Party Processors are contractually bound by us to keep the information confidential. All information provided to Third Party Processors is used by them only to carry out the service they are providing for the Handmade Soap Company.

</p>
    </div>
    </div>
  </div>
   </main>>

</body>

 <?php
        include_once 'footer.php';
?>
</html>