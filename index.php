 <?php
        include_once 'header.php';
?>
 

<section class="index-banner">
               
                <div class ="vertical-center"> 
                    <h2>Rosie's Soaps</h2>
                    <h1>Homemade Wholesome Soaps</h1>
                </div>
            </section>
  <br>

  <section class="description-index">

<div class="container-fluid">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <p class="lead text-center">We sell Soaps, Bath Bombs and a wide range of natural and organic beauty products, <br> all made in Ireland</p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-2">
      <a href="shop.php"><button type="button" class="btn btn-outline-secondary btn-lg center-block">Shop</button></a>
  </div>
</div>
</section>

<!--Welcome Section -->

<div class="container-fluid padding">
  <div class="row welcome text-center">
    <div class="col-12">
      <h1 class="display-4"></h1>
  </div>
  <hr>
  <div class="col-12">
    <p class="lead"></p>
</div>
</div>
</div>


 <?php
        include_once 'footer.php';
?>
 

