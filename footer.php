   <link href="footer.css" rel="stylesheet">
<footer class="flex-rw" id="ffff">
  
  <ul class="footer-list-top">
    <li>
      <h4 class="footer-list-header">About Rosie's Soaps</h4></li>
    <li><a href='about.php' class="generic-anchor footer-list-anchor" itemprop="significantLink">GET TO KNOW US</a></li>

    <li><a href='retailer.php' itemprop="significantLink" class="generic-anchor footer-list-anchor">BECOME A STOCKIST</a></li>

    <li><a href='jobs.php' itemprop="significantLink" class="generic-anchor footer-list-anchor">JOB OPENINGS</a></li>
  </ul>
  <ul class="footer-list-top">
    <li>
      <h4 class="footer-list-header">Customer Care</h4></li>


    <li><a href='delivery.php' class="generic-anchor footer-list-anchor">DELIVERY & RETURNS</a></li>
    <li><a href='terms.php' class="generic-anchor footer-list-anchor">TERMS & CONDITONS</a></li>
    <li><a href='signup.php' class="generic-anchor footer-list-anchor">CREATE YOUR ACCOUNT</a></li>
  </ul>
  <ul class="footer-list-top">
    <li id='help'>
      <h4 class="footer-list-header">Help</h4></li>
    <li><a href='contact.php' id="linky" class="generic-anchor footer-list-anchor" itemprop="significantLink">CONTACT</a></li>
    <li><a href='faq.php' class="generic-anchor footer-list-anchor" itemprop="significantLink">FAQ</a></li>
    <li id='order-tracking'><a href='orders.php' itemprop="significantLink" class="generic-anchor footer-list-anchor">ORDER STATUS</a></li>
  </ul>
  <section class="footer-social-section flex-rw">
      <span class="footer-social-overlap footer-social-connect">
      CONNECT <span class="footer-social-small">with</span> US
      </span>
      <span class="footer-social-overlap footer-social-icons-wrapper">
      <a href="https://www.pinterest.com" class="generic-anchor" target="_blank" title="Pinterest" itemprop="significantLink"><i class="fa fa-pinterest"></i></a>
      <a href="https://www.facebook.com" class="generic-anchor" target="_blank" title="Facebook" itemprop="significantLink"><i class="fa fa-facebook"></i></a>
      <a href="https://twitter.com/" class="generic-anchor" target="_blank" title="Twitter" itemprop="significantLink"><i class="fa fa-twitter"></i></a>
      <a href="http://instagram.com/" class="generic-anchor" target="_blank" title="Instagram" itemprop="significantLink"><i class="fa fa-instagram"></i></a>
      <a href="https://www.youtube.com/channel/" class="generic-anchor" target="_blank" title="Youtube" itemprop="significantLink"><i class="fa fa-youtube"></i></a>
      <a href="https://plus.google.com/" class="generic-anchor" target="_blank" title="Google Plus" itemprop="significantLink"><i class="fa fa-google-plus"></i></a>
      </span>
  </section>
  <section class="footer-bottom-section flex-rw">
<div class="footer-bottom-wrapper">   
<i class="fa fa-copyright" role="copyright">
 
</i>2018 Rosie's Soaps <address class="footer-address" role="company address">Dublin, Ireland</address><span class="footer-bottom-rights"> - All Rights Reserved - </span>
    </div>
    <div class="footer-bottom-wrapper">
    <a href="/terms-of-use.html" class="generic-anchor" rel="nofollow">Terms</a> | <a href="/privacy-policy.html" class="generic-anchor" rel="nofollow">Privacy</a>
      </div>
  </section>
</footer>


</body>
</html>






