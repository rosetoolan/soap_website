<!DOCTYPE html>
<html lang="en">
<head>
  
 <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="style.css" rel="stylesheet">
</head>
<body>
 

 <?php
        include_once 'header.php';
?>



 <main class ="floral-img-3">

<div class="container-fluid padding section-about">
  <div class="row padding">
    <div class="col-lg-12">
      <h2>Become a stockist</h2>
      <p>

If you’re interested in coming on board, and stocking some gorgeous Handmade Soap Company goodies, please get in touch. We’d like to know a little bit about you, what your store or stores are like. You could tell us why you think our brand is a good fit for yours, and your customers, and maybe what other kinds of brands that you stock.

<BR>

We’d be delighted to chat and will get back to you as soon as we can.

<BR>

Email us at info@rosiessoaps.ie or call us on (353) 41 9 4704.
</p>
    </div>
    </div>
  </div>
   </main>>

</body>

 <?php
        include_once 'footer.php';
?>
</html>