<style>

.order-table{
	background-color:#F7D6E6;
	margin-top: 20px;
	color:#463080;
	font-size:20px;
	font-weight:300;
}



#side-menu{
  font-family: 'Amatic SC', cursive;
  font-size: 30px;
  font-weight:700px;
  background-color:#F7D6E6;
  color:#463080;
  line-height:63px;
  text-shadow: 4px 4px 16px #A87798;

}

#side-menu:hover{
  font-family: 'Amatic SC', cursive;
  font-size: 30px;
  font-weight:700px;
  background-color:#F7D6E6;
  color:#463080;
  line-height:63px;
  text-shadow: 4px 4px 16px #A87798;

}

.list-group-item {
	font-family: 'Amatic SC', cursive;
  font-size: 30px;
  font-weight:700px;
  background-color:#F7D6E6;
  color:#463080;
  line-height:63px;
  text-shadow: 4px 4px 16px #A87798;

	}

.thumbnail{
font-family: 'Amatic SC', cursive;
  font-size: 30px;
  font-weight:700px;
  background-color:#F7D6E6;
  color:#463080;
  line-height:63px;
  text-shadow: 4px 4px 16px #A87798;

}

.thumbnail button{
background: #463080;
    color:white;
    text-align: center;
    padding: 12px;
    text-decoration: none;
    display: block;
    border-radius: 3px;
    font-size: 15px;
    margin: 15px 0 15px 0;


}

.thumbnail button:hover {
    background: #EBB0D0;
    color:#463080;
  
 
  
}
</style> 
 
 <?php
        include_once 'header.php';
?>
 

<!--
            <div class="modal-footer">
                <div style="padding:10px"></div>
            </div>
-->
<main class ="floral-img-3">

<div class="container-fluid padding section-about">

      <div class="row side-menu">
 <div class="col-sm-4 col-md-3">
 <h2 >Manage Orders</h2>
 <div class="list-group">
 	<a href="account.php" class="list-group-item">My Profile</a>
 <a href="orders.php" class="list-group-item">All Orders</a>
 
 </div>

 </div>

 <div class="col-sm-8 col-md-9">

  <div class="row">

    <h3>My Payment Information</h3>
  </div>
  <div class="borderBox">
    <div class="tabler">
      <h3>Credit Card Authorization</h3>
      <a href="#" class="edit grayLG fright">Edit</a>
    <table>
      <tbody>
      <tr>
        <td>
          Credit Card Number
        </td>
        <td>
          xxxxxxxxxxxx1111
        </td>
      </tr>
      <tr>
        <td>
          Credit Card Type
        </td>
        <td>
          Visa / Mastercard
        </td>
      </tr>
      <tr>
        <td>
          Credit Card Expiration
        </td>
        <td>
          0916
        </td>
      </tr>
      </tbody>
    </table>
    </div> 
    
    <div class="tabler">
      <h3>Billing Information</h3>
      <a href="#" class="edit grayLG fright">Edit</a>
    <table>
      <tbody>
      <tr>
        <td>
          Name
        </td>
        <td>
          Robert M. Hamilton
        </td>
      </tr>
      <tr>
        <td>
          Street Address
        </td>
        <td>
          1627 Rose Street
        </td>
      </tr>
      <tr>
        <td>
          Street Address 2
        </td>
        <td>
          
        </td>
      </tr>
        <tr>
        <td>
          City
        </td>
        <td>
          Manhattan
        </td>
      </tr>
        <tr>
        <td>
          State (Province)
        </td>
        <td>
          Kansas
        </td>
      </tr>
        <tr>
        <td>
          Postal Code
        </td>
        <td>
          66502
        </td>
      </tr>
        <tr>
        <td>
          Country
        </td>
        <td>
          United States
        </td>
      </tr>
        <tr>
        <td>
          Phone Number
        </td>
        <td>
          716-678-7424
        </td>
      </tr>
      </tbody>
    </table>
    </div> 
  </div>
</div>



</div>

</div>
 



 
 <?php
        include_once 'footer.php';
?>
