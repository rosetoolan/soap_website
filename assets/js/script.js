$(function(){
	var photos = [
		'img/697aa6b2270a44f655f7c1994627a065.jpg',
		'img/blackberry-vanilla-soap-natural-soap-handmade-soap-spa-soap-cold-process-soap-homemade-soap-artisan-soap-new-hampshire-soap-spa-bar.jpg',
		'img/697aa6b2270a44f655f7c1994627a065.jpg',
		'img/blackberry-vanilla-soap-natural-soap-handmade-soap-spa-soap-cold-process-soap-homemade-soap-artisan-soap-new-hampshire-soap-spa-bar.jpg',
		'img/697aa6b2270a44f655f7c1994627a065.jpg',
		'img/blackberry-vanilla-soap-natural-soap-handmade-soap-spa-soap-cold-process-soap-homemade-soap-artisan-soap-new-hampshire-soap-spa-bar.jpg'
	];
	
	var slideshow = $('#slideShow').bubbleSlideshow(photos);

	$(window).load(function(){
		slideshow.autoAdvance(5000);
	});
	
	// Other valid method calls:
	
	// slideshow.showNext();
	// slideshow.showPrev();
	// slideshow.stopAutoAdvance();
});
