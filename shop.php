<style>

.products button {
 
    background: #463080;
    color:white;
    text-align: center;
    padding: 12px;
    text-decoration: none;
    display: block;
    border-radius: 3px;
    font-size: 20px;
    margin: 25px 0 15px 0;

}

.products button:hover {
    background: #EBB0D0;
    color:#463080;
    text-align: center;
    padding: 12px;
    text-decoration: none;
    display: block;
    border-radius: 3px;
    font-size: 20px;
    margin: 25px 0 15px 0;
  
}
  .categories-container h3{

  font-family: 'Amatic SC', cursive;
  font-size: 50px;
  color:#8836FE;
  line-height:63px;
    text-shadow: 4px 4px 16px #A87798;
  
}


a.list-group{
  background-color:#F7D6E6;


}



a.list-group:hover{
  background-color:#F7D6E6;
  color:#463080;

}

a.list-group-item{
  font-family: 'Amatic SC', cursive;
  font-size: 30px;
  font-weight:700px;
  background-color:#F7D6E6;
  color:#463080;
  line-height:63px;
  text-shadow: 4px 4px 16px #A87798;

}




</style>
 <?php
        include_once 'header.php';
?>

<br>
 

<!-- Carousel -->

<div class="container" id="shop-conatiner">
  <div class="col-md-12">
  <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
  
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="img/blackberry-vanilla-soap-natural-soap-handmade-soap-spa-soap-cold-process-soap-homemade-soap-artisan-soap-new-hampshire-soap-spa-bar.jpg" alt="Soaps" style="width:100%;">
        <div class="carousel-caption">
          <h1 class="display-2">Soaps</h1>
          <button href="soaps.php" type="button" id ="carousel-button"class="btn btn-outline-light btn-lg">Shop Soaps</button>
        </div>
      </div>
    
      <div class="item">
        <img src="img/8.jpg" alt="Bath Bombs" style="width:100%;">
        <div class="carousel-caption">
          <h1 class="display-2">Bath Bombs</h1>
          <button heref="bathbombs.php" type="button" id ="carousel-button"class="btn btn-outline-light btn-lg">Shop Bath Bombs</button>
        </div>
      </div>

      <div class="item">
        <img src="img/240_F_166077539_9enl8VNrb9wlSTVS2VTsQUMTO0rD1Jc0.jpg" alt="Skin-care" style="width:100%;">
        <div class="carousel-caption">
          <h1 class="display-2">Skincare</h1>
          <button href="skincare.php"type="button" id ="carousel-button"class="btn btn-outline-light btn-lg">Shop Skincare</button>
        </div>
      </div>

       <div class="item">
        <img src="img/Hair-care.jpg" alt="Hair-care" style="width:100%;">
        <div class="carousel-caption">
          <h1 class="display-2">Haircare</h1>
          <button href="haircare.php" type="button" id ="carousel-button" class="btn btn-outline-light btn-lg">Shop Haircare</button>
        </div>
      </div>

    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  </div>
</div>
</div>



<!--Welcome Section -->

<div class="container-fluid padding" id ="welcome-featured">
  <div class="row welcome text-center">
    <div class="col-12">
      <h1 class="display-4">Featured Products</h1>
  </div>
  <hr>
  <div class="col-12">
    <p class="lead"></p>
</div>
</div>
</div>



<!-- Main Content -->
<div class="container" id="featured-container">
  <div class="row text-center">
    <div class="col-md-3 featured-prod">
      <a href ="#"><img src="img/diy-soap-lemon-cake-homemade-soap-recipes.jpg"id ="featured-product-img" class="img-fluid"></a>
      <h3 id ="featured-product-title">Lemon Cake Soap</h3>
      <div class="text-muted">
      <p id ="featured-product-desc">One of Rosie's best sellers,this soap will leave you smelling lemony fresh</p>
    </div>
    <button type="button" class="btn btn-primary btn-lg">Buy</button>
    </div>

    <div class="col-md-3 featured-prod">
      <a href ="#"><img src="img/il_570xN.258056972.jpg"id ="featured-product-img" class="img-fluid"></a>
      <h3 id ="featured-product-title">Lavendar Bath Bomb</h3>
      <div class="text-muted">
      <p align ="justify" id ="featured-product-desc">Enjoy a relaxing bath with the calming power of lavender. These bath bombs have all the skin loving oils that will rejuvenate and revitalize your skin. With the soothing scent of lavender to give your mind a break from the stresses of the day, and if used in the evening should help you sleep.</p>
    </div>
    <button type="button" class="btn btn-primary btn-lg">Buy</button>
    </div>
    <div class="col-md-3 featured-prod">
      <a href ="#"><img src="img/levanduľový-olej-1.jpg"id ="featured-product-img" class="img-fluid"></a>
      <h3 id ="featured-product-title">Lavendar Shampoo</h3>
      <div class="text-muted">
      <p align ="justify" id ="featured-product-desc">Our Lavender Shampoo is made with shea butter, dried lavender and lavender essential oils. Lavender is used in aromatherapy to relax and relieve stress. The dried lavender offers soft exfoliation to help increase circulation.</p>
    </div>
    <button type="button" class="btn btn-primary btn-lg">Buy</button>
    </div>
    <div class="col-md-3 featured-prod">
      <a href ="#"><img src="img/Mermaid-Tail-Cold-Process.jpg"id ="featured-product-img" class="img-fluid"></a>
      <h3 id ="featured-product-title">Mermaid Soap</h3>
      <div class="text-muted">
      <p align ="justify" id ="featured-product-desc">A treasure of blended raspberry, cranberry and pomegranate with citrus including yuzu, citron and neroli.)</p>
    </div>
    <button type="button" class="btn btn-primary btn-lg">Buy</button>
    </div>
    </div>
  </div>

  <!-- Shop Items -->

<div class="container-fluid padding" id ="shop-items">
  <div class="row shopitems text-center">
    <div class="col-12">
      <h1 class="display-4">Shop All Categories</h1>
  </div>
  <hr>


<div class="container categories-container">

 <div class="row">
 <div class="col-sm-4 col-md-3">
 <h3>Categories</h3>
 <div class="list-group">
  <a href="shop.php" class="list-group-item">All Categories</a>
 <a href="soaps.php" class="list-group-item">Soaps</a>
 <a href="bathbombs.php" class="list-group-item">Bath Bombs</a>
 <a href="haircare.php" class="list-group-item">Hair Care</a>
 <a href="skincare.php" class="list-group-item">Skin Care</a>
 </div>
</div>
<div class="col-sm-8 col-md-9">

  <div class="row">

        <?php

        $connect = mysqli_connect('localhost', 'root', '', 'cart');
        $query = 'SELECT * FROM products ORDER by id ASC';
        $result = mysqli_query($connect, $query);

        if ($result):
            if(mysqli_num_rows($result)>0):
                while($product = mysqli_fetch_assoc($result)):
                //print_r($product);
                ?>
        
                <div class="col-sm-6 col-md-4" >
                    <form method="post" action="cart.php?action=add&id=<?php echo $product['id']; ?>">
                        <div class="products">
                            <img src="<?php echo $product['image']; ?>" class="img-responsive" /> 
                            <h4 class="text-info"><?php echo $product['name']; ?></h4>
                            <h4><?php echo $product['price']; ?></h4>
                            <input type="text" name="quantity" class="form-control" value="1" />
                            <input type="hidden" name="name" value="<?php echo $product['name']; ?>" />
                            <input type="hidden" name="price" value="<?php echo $product['price']; ?>" />
                            <input type="submit" name="add_to_cart" style="margin: 5px 200px 0px 0px" class="btn btn-info" value="Add to Cart" />
                            
                           <a style="margin-top:2px;"href="productpage.php?id=<?php echo $product['id'];?>"><button type="button" class="btn btn-outline-secondary btn-lg center-block">View Product</button></a>

                               
                        </div>
                    </form>
                </div>
                <?php
                endwhile;
            endif;
        endif;   
        ?>
 
  </div>

  </div>



 </div>

 </div>

 </div><!-- /.container class with content as the id-->





 <?php
        include_once 'footer.php';
?>

