<!DOCTYPE html>
<html lang="en">
<head>
  
 <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="style.css" rel="stylesheet">
</head>
<body>
 

 <?php
        include_once 'header.php';
?>



 <main class ="floral-img-3">

<div class="container-fluid padding section-about">
  <div class="row padding">
    <div class="col-lg-12">
      <h2>Join our Team</h2>
      <p>

We’re a tight knit bunch at Rosie's Soaps, with a strict ‘no nasties’ policy. That applies to our ingredients and our staff.

<br>

If you think you’d be a good fit for us, and you share our passion for creating natural, handmade loveliness, do get in touch.

<br>

We’d love to hear from you.

Email us at info@rosiessoaps.ie .
</p>
    </div>
    </div>
  </div>
   </main>>

</body>

 <?php
        include_once 'footer.php';
?>
</html>