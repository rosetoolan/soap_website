<!DOCTYPE html>
<html lang="en">
<head>
  
 <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="style.css" rel="stylesheet">
</head>
<body>
 

 <?php
        include_once 'header.php';
?>



 <main class ="floral-img-3">

<div class="container-fluid padding section-about">
  <div class="row padding">
    <div class="col-lg-12">

      <h2>FAQ (Frequently Asked Questions)</h2>
<h3>What makes your soaps so good?</h3>
<p>I don’t use any animal fats in my soap, they are vegan. Only high quality oils are used. I make them scented with only essential oils, no synthetic oils are used. In my handmade soaps there are no harsh detergents as you usually find in mass produced soaps, they retain all the glycerin. Not only do they smell great but they also lather up nicely and moisturize your skin.</p>


<h3>>How long will your soaps last?</h3>
<p>Our soaps will keep for up to a year but we recommend you use them within 6 months.</P>


<p>I have sensitive skin, can I use your handmade soap?
We highly recommend you try handmade soap. All our soaps have natural beneficial oils as well as glycerin to help moisturize your skin. We do not use any synthetic’s, dyes, preservatives or petroleum products in our products. Our process produces a very mild soap. We think you will notice the difference</p>


<h3>Is your soap "Tearless"?</h3>
<p>As in the case with all real soap, our soap is not “Tearless.” Care must be taken not to get in the eyes.</p>


<h3>Does your soap contain lye?</h3>
<p>True soap cannot be made without lye – Sodium hydroxide for hard soap and potassium hydroxide for liquid soaps. Technically speaking, soap is actually an alkali salt of a fatty acid. The salt is formed as a result of the chemical reaction between an alkali [lye] and an acid [fatty acids found in fats and oils].
When the lye solution and liquid are mixed with the fats and oils, the molecules of the alkali join with the molecules of the fatty acids and the final result is soap. The chemical process is complete and all lye is reacted out. This is referred to as saponified oils.</p>


<h3>Do you have a store front? Can I buy in person?</h3>
<p>This is an an Internet-based store. I do sell from my shop, if you are local just email me or call and we will see what we can do. We also are participating in local art’s and craft shows and farmer’s markets throughout the year. We will update our Events calendar as soon as dates & times are established.</p>


<h3>I don't like to use PayPal. Can I pay another way?</h3>
<p>We use Paypal for your security, but if you don’t like to use PayPal, you can pay by check or money order. Contact us for details.</p>


<h3>Commonsense Precautions</h3>
<p>As with any product that touches our skin, please discontinue use if irritation develops. Any product, even when made with the most natural, gentle ingredients can be irritating to certain sensitive individuals.</p>


<h3>Disclaimers</h3>
<p>This notice is required by the Federal Food Drug & Cosmetic Act: The contents of this website are for informational purposes only. The content is not intended to be a substitute for professional medical advice, diagnosis, or treatment. Always seek the advice of your physician or other qualified health provider with any questions you may have regarding a medical condition. Always consult your doctor if you are pregnant, lactating or have other health conditions before applying these products. Keep out of eyes & mucous membranes. All of our products should be used with the same care and caution as any other bath or household product that you use. Handmade assumes no liability for claims arising from the misuse of our products.</p>
    </div>
    </div>
  </div>
   </main>>

</body>

 <?php
        include_once 'footer.php';
?>
</html>