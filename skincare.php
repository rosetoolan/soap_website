<style>
 .categories-container h3{

  font-family: 'Amatic SC', cursive;
  font-size: 50px;
  color:#8836FE;
  line-height:63px;
    text-shadow: 4px 4px 16px #A87798;
  
}


a.list-group-item{
  font-family: 'Amatic SC', cursive;
  font-size: 30px;
  font-weight:700px;
  background-color:#F7D6E6;
  color:#463080;
  line-height:63px;
  text-shadow: 4px 4px 16px #A87798;

}

a.list-group-item:hover{
  font-family: 'Amatic SC', cursive;
  font-size: 30px;
  font-weight:700px;
  background-color:#F7D6E6;
  color:#463080;
  line-height:63px;
  text-shadow: 4px 4px 16px #A87798;

}

.products button {
 
    background: #463080;
    color:white;
    text-align: center;
    padding: 12px;
    text-decoration: none;
    display: block;
    border-radius: 3px;
    font-size: 20px;
    margin: 25px 0 15px 0;

}

.products button:hover {
    background: #EBB0D0;
    color:#463080;
    text-align: center;
    padding: 12px;
    text-decoration: none;
    display: block;
    border-radius: 3px;
    font-size: 20px;
    margin: 25px 0 15px 0;
  
}





</style>
 <?php
        include_once 'header.php';

?>


<div class="container-fluid padding" id ="welcome-featured">
  <div class="row welcome text-center">
    <div class="col-12">
      <h1 class="display-4">Skincare</h1>
  </div>
  <hr>
  <div class="col-12">
    <p class="lead"></p>
</div>
</div>
</div>

<div class="container categories-container">

 <div class="row">
 <div class="col-sm-4 col-md-3">
 <h3>Categories</h3>
 <div class="list-group">
  <a href="shop.php" class="list-group-item">All Categories</a>
 <a href="soaps.php" class="list-group-item">Soaps</a>
 <a href="bathbombs.php" class="list-group-item">Bath Bombs</a>
 <a href="haircare.php" class="list-group-item">Hair Care</a>
 <a href="skincare.php" class="list-group-item">Skin Care</a>
 </div>
</div>
<div class="col-sm-8 col-md-9">

  <div class="row">


        <?php

        $connect = mysqli_connect('localhost', 'root', '', 'cart');
        $query = 'SELECT * FROM products WHERE categories="skincare" ORDER by id ASC';
        $result = mysqli_query($connect, $query);

       if ($result):
            if(mysqli_num_rows($result)>0):
                while($product = mysqli_fetch_assoc($result)):
                //print_r($product);
                ?>
        
                <div class="col-sm-6 col-md-4" >
                    <form method="post" action="cart.php?action=add&id=<?php echo $product['id']; ?>">
                        <div class="products">
                            <img src="<?php echo $product['image']; ?>" class="img-responsive" /> 
                            <h4 class="text-info"><?php echo $product['name']; ?></h4>
                            <h4><?php echo $product['price']; ?></h4>
                            <input type="text" name="quantity" class="form-control" value="1" />
                            <input type="hidden" name="name" value="<?php echo $product['name']; ?>" />
                            <input type="hidden" name="price" value="<?php echo $product['price']; ?>" />
                           <input type="submit" name="add_to_cart" style="margin-top:5px;" class="btn btn-info" value="Add to Cart" />
                           <a style="margin-top:5px;"href="productpage.php?id=<?php echo $product['id'];?>"><button type="button" class="btn btn-outline-secondary btn-lg center-block">View Product</button></a>
                               
                               
                        </div>
                    </form>
                </div>
                <?php
                endwhile;
            endif;
        endif;   
        ?>
 
  </div>
</div>

  </div>


 <?php
        include_once 'footer.php';
?>