<!DOCTYPE html>
<html lang="en">
<head>
  
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="style.css" rel="stylesheet">
  <style>
.main-container{
   padding-top: 40px;

}

.main-wrapper h2{
    font-family:'Amatic SC', cursive;
    font-size: 50px;
    color: ##93939F;
    line-height: 50px;
    text-align: center;
    text-shadow: 4px 4px 16px #140B29;

}

.signup-form{
    width:400px; 
    margin: 0 auto;
    padding-top: 30px;

}
.signup-form input{
    width:90%;
    height: 40px;
    padding: 0px 5%;
    border: none;
    margin-bottom: 4px;
    background-color: #F7F5FC;
    font-family:'Amatic SC', cursive;
    font-size: 16px;
    color: #111;
    line-height: 40px;

}

.signup-form button{
    display: block;
    width:30%;
    height: 40px;
    border: none;
    margin-bottom: 4px;
    background-color: #F7F5FC;
    font-family:'Amatic SC', cursive;
    font-size: 16px;
    color: #111;
    line-height: 40px;

}

.signup-form button:hover{
    background-color: #EBB0D0;
    border: 5px solid;
    border-color: #F7F5FC;
    cursor: pointer;
    line-height: 20px;

}
.floral-img{
    width:100%;
    height:700px;
    background-image: url('img/Peony_Adisorn-Pornsirikarn_watercolor-flowers-1024x754.jpg');
    background-repeat: no-repeat;
    background-position: center;
    background-size:1000px 700px;
    display:table;
    line-height: 60px;

    
</style>
</head>


<body>


 

 <?php
        include_once 'header.php';
?>
 
 <br>

<main class ="floral-img">

<section class ="main-container">
    <div class ="main-wrapper">
        <h2>Signup</h2>
        <form class="signup-form" action="includes/signup.inc.php" method="POST">
            <input type="text" name="first" placeholder="firstname">
            <input type="text" name="last" placeholder="lastname">
            <input type="text" name="email" placeholder="email">
            <input type="text" name="uid" placeholder="username">
            <input type="password" name="pwd" placeholder="password">
            <button type="submit" name="submit">Signup</button>
        </form>
    </div>
</section>
</main>

<div class= "container">
  
</div>
<br>

 <?php
        include_once 'footer.php';
?>
