<!DOCTYPE html>
<html lang="en">
<head>
  
 <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="style.css" rel="stylesheet">
</head>
<body>
 

 <?php
        include_once 'header.php';
?>



 <main class ="floral-img-3">

<div class="container-fluid padding section-about">
  <div class="row padding">
    <div class="col-lg-12">
      <h2>DELIVERY AND RETURNS</h2>

<h3>DELIVERY </h3>

<p>We will deliver within the Republic of Ireland  and Northern Ireland within 3 to 5 working days, where this is practical.</p>

<p>To mainland UK we will deliver within 5 to 7 working days.</p>

<p>Our delivery to mainland Europe and further afield will vary depending on where you are.</p>

<p>Mainland Europe and International Delivery Times: Will vary depending on the destination.</p>

<p>Products on our site are subject to availability and delays may sometimes occur that are outside of our control and consequently any dates specified for delivery are therefore approximate only. When you purchase on our site we will send you a quotation and any products not available at the time of your order will be sent as soon as possible – as soon as they roll off the line!</p>


<h3> DAMAGED/REJECTED GOODS </h3>

<p>As the customer you must inspect the goods within a reasonable time after their receipt and you’ll be deemed to have accepted the goods unless within 7 days after their receipt you notify us at info@rosiessoaps.com that the goods are rejected. (This does not affect your statutory rights).</p>

<p>If you have not notified us as above, we’ll consider the products as being of satisfactory quality for you fit for their purposes, and may not accept any reject at a later date.</p>

<p>We want you to receive your Rosie's Soaps goods in perfect condition – as they left us. If goods, purchased from us through this website have been received damaged we will refund or exchange them, if you return them to us, with their original packaging within 7 days of receiving them to our address:</p>

<p>Rosie's Soaps, Olde Dock, Little Ship Street, Dublin 8, Co. Dublin, Ireland</p>


<h3>RETURNS PROCEDURE</h3>

<p>You, the Customer must send all returns within 7 days of receipt to: Rosie's Soaps, Olde Dock, Little Ship Street, Dublin 8, Co. Dublin, Ireland. Where the rejection of the goods is due to a defect or discrepancy in the order, you’re entitled to a full refund or replacement.<p>

<p>The card used for the purchase will be credited with full original purchase and delivery costs. The faulty or damaged product must be returned to The Handmade Soap Company before the refund or replacement can be issued. When returning items we’d recommend you hold on to proof of postage – we can’t take responsibility for any parcels that might get lost in transit to us.</p>


<h3>CANCELLATIONS<h3>

<p>While we are disappointed when it happens, we understand that sometimes you need to change your mind and cancel an order. Cancellations are only accepted before the order has been dispatched. If the order has been dispatched prior to the cancellation, we can’t then cancel it , and the order will be considered valid and you, the Customer will remain liable for the full payment.</p>

<p>If you must – we understand. Please make any cancellations by phone to +353 (41) 988 4704 or by email: info@rosiessoaps.com  . If you must!</p>
    </div>
    </div>
  </div>
   </main>>

</body>

 <?php
        include_once 'footer.php';
?>
</html>